#!/usr/bin/env python3

import os
import sys
import csv
import codecs
import datetime
import argparse
import traceback
import unicodedata
import googleapiclient.discovery

API_SERVICE_NAME = "youtube"
API_VERSION = "v3"
COMMENT_KIND = "youtube#comment"

JSON_DATE_FMT = "%Y-%m-%dT%H:%M:%SZ"
DATE_FMT = "%d%m%Y"
CSV_DATE_FMT = "%d/%m/%Y"

class VideoItem:
    def __init__(self, id:int, youtubeId:str, maxDate:datetime.datetime):
        self.id = id
        self.youtubeId = youtubeId
        self.maxDate = maxDate
        self.publishDate = datetime.date.today()
        self.totalCommentsInPeriod = 0

        self.maxDate += datetime.timedelta(days=1.0)

    def to_csv_string(self):
        return f"{self.id};{self.youtubeId};{datetime.datetime.strftime(self.publishDate, CSV_DATE_FMT)};{datetime.datetime.strftime(self.maxDate - datetime.timedelta(days=1.0), CSV_DATE_FMT)};{self.totalCommentsInPeriod}\r\n"

class PagedGoogleApiRequest:
    def __init__(self, requestCreator):
        self._createRequest = requestCreator
        self._response:dict
        self._pageToken:str

        self._response = None
        self._pageToken = None

    def response(self):
        return self._response

    def move_next(self):
        if not self._response:
            return True

        self._pageToken = None
        if "nextPageToken" in self._response:
            self._pageToken = self._response["nextPageToken"]

        return self._pageToken != None

    def execute(self):
        self._response = execute_googleapi_request(
            self._createRequest(self._pageToken)
        )

        return self._response is not None

def count_comments_for_video(youtube:object, video:VideoItem):
    # Disable OAuthlib's HTTPS verification when running locally.
    # *DO NOT* leave this option enabled in production.
    # os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = "1"

    existsRequest = youtube.videos().list(part="snippet", id=video.youtubeId)
    existsResponse = execute_googleapi_request(existsRequest)
    if existsResponse is None or existsResponse["pageInfo"]["totalResults"] == 0:
        raise Exception("Error: video not found (%s)" % video.youtubeId)

    video.totalCommentsInPeriod = 0
    video.publishDate = datetime.datetime.strptime(existsResponse["items"][0]["snippet"]["publishedAt"], JSON_DATE_FMT)

    threadsRequest = PagedGoogleApiRequest(
        lambda pageToken:
            youtube.commentThreads().list(
                order = "time", # "relevance",
                part = "snippet",
                pageToken = pageToken,
                videoId = video.youtubeId
            )
    )

    while threadsRequest.move_next():
        if not threadsRequest.execute():
            break

        response = threadsRequest.response()

        for thread in response["items"]:
            parentComment = thread["snippet"]["topLevelComment"]
            commentDate = datetime.datetime.strptime(parentComment["snippet"]["publishedAt"], JSON_DATE_FMT)

            if commentDate > video.maxDate:
                continue

            video.totalCommentsInPeriod += 1

            if thread["snippet"]["totalReplyCount"] > 0:
                repliesRequest = PagedGoogleApiRequest(
                    lambda pageToken:
                        youtube.comments().list(
                            part = "snippet",
                            pageToken = pageToken,
                            parentId = parentComment["id"]
                        )
                )

                while repliesRequest.move_next():
                    if not repliesRequest.execute():
                        break

                    repliesResponse = repliesRequest.response()

                    repliesResponse["items"].sort(reverse=True, key=get_comment_publish_date)

                    for reply in repliesResponse["items"]:
                        replyDate = datetime.datetime.strptime(reply["snippet"]["publishedAt"], JSON_DATE_FMT)

                        if replyDate > video.maxDate:
                            continue

                        video.totalCommentsInPeriod += 1

def execute_googleapi_request(request):
        return request.execute()

def get_comment_publish_date(commentObj):
    if "kind" not in commentObj or commentObj["kind"] != COMMENT_KIND:
        return None

    return commentObj["snippet"]["publishedAt"]

def get_file_start_without_bom(filePath:str):
    fileStart = 0
    with open(filePath, "rb") as file:
        bomTest = file.read(len(codecs.BOM_UTF8))
        if bomTest.startswith(codecs.BOM_UTF8):
            fileStart = len(codecs.BOM_UTF8)

    return fileStart

if __name__ == "__main__":
    exit_code = 0
    parser = argparse.ArgumentParser(description="YouTube comment counter")
    parser.add_argument("--input", required=True, type=str, help="File containing CSV list of tuples (videoId[integer], youtubeId[string], maxDate[dd.mm.YY])")

    args = parser.parse_args()

    with open("developer.key", "rb") as devKeyFile:
        devKey = devKeyFile.readline()

    youtube = googleapiclient.discovery.build(
        API_SERVICE_NAME,
        API_VERSION,
        developerKey = devKey
    )

    fileStart = get_file_start_without_bom(args.input)
    inputFile = open(args.input, "r")

    inputFile.seek(fileStart, 0)
    dialect = csv.Sniffer().sniff(inputFile.readline())

    inputFile.seek(fileStart, 0)
    inputReader = csv.reader(inputFile, dialect=dialect)

    videos = []
    currentLine = 0
    try:
        for row in inputReader:
            currentLine += 1
            videos.append(VideoItem(id=int(row[0]), youtubeId=row[1], maxDate=datetime.datetime.strptime(row[2], CSV_DATE_FMT)))

    except Exception as err:
        print(f"Error while processing input csv file at line {currentLine}:")
        print(err)
        traceback.print_tb(err.__traceback__)
        exit_code = 1

    finally:
        inputFile.close()
        if exit_code:
            exit(exit_code)

    fileName, fileExt = os.path.splitext(args.input)
    outFile = open(f"{fileName}_out{fileExt}", "w")

    try:
        for item in videos:
            sys.stdout.write("Processing video %d... " % item.id)
            sys.stdout.flush()
            count_comments_for_video(youtube, item)
            outFile.write(item.to_csv_string())
            sys.stdout.write("done\r\n")
            sys.stdout.flush()

        outFile.flush()
        outFile.close()

    except Exception as err:
        outFile.close()
        os.remove(outFile.path)

        sys.stdout.write("\r\n")
        sys.stdout.flush()
        print("Processing stopped due to an error:", flush=True)
        print(err)
        traceback.print_tb(err.__traceback__)
        exit_code = 1

    exit(exit_code)